import * as React from "react";
import * as ReactDOM from "react-dom";
import { Router } from 'react-router-dom';
import { AppContainer } from 'react-hot-loader';
import "./assets/global.scss";
import { App } from './containers/App';
import 'antd/dist/antd.css';
import 'antd/dist/antd.less';
import { createBrowserHistory } from "history";

const render = () => { 

  const history = createBrowserHistory();

  return ReactDOM.render(
    <AppContainer>
      <Router history={history}>
        <App/>
      </Router>
    </AppContainer>,
    document.getElementById("app"),
  );
}


render();