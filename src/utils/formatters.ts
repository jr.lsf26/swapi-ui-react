export const heightFormatter = (height: string) => { 
    return height.length > 2 ? `${height.substring(0,1)},${height.substring(1,height.length)}m` : `${height}cm`
}

export const massFormatter = (mass: string) => { 
    return `${mass}kg`
}

export const capitalize = (text: string) => { 
    return text?.charAt(0).toUpperCase() + text?.slice(1);
}