
export interface ICharacterInfo{
    name: string;
    height: string;
    mass: string; 
    hair_color: string;
    skin_color: string;
    eye_color: string;
    birth_year: string;
    gender: string;
    homeworld: IWorldInfo;
    films: IFilmInfo[];
    species: string[];
    vehicles: IVehicleInfo[];
    starships: string[];
}

export interface IWorldInfo{
    name: string;
}

export interface IFilmInfo{
    title: string;
    releaseDate: string;
}

export interface IVehicleInfo{
    name: string;
    model: string;
    crew: string;
    class: string;
}

export interface IStarshipsInfo{
    name: string;
    model: string;
    crew: string;
    rating: string;
    class: string;
}