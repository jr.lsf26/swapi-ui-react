import * as React from 'react';
import { Header } from '../../components/Header';
import { Row, Col, Alert, Spin, Pagination} from 'antd';
import Search from 'antd/lib/input/Search';
import { CharacterCard } from '../../components/Card';
import { ICharacterInfo } from '../../utils/interfaces';
import { EmptyResult } from '../../components/EmptyResult';

const style = require('./style.scss');

const API = "https://swapi.co/api/";

export const Home = () => {

    const [query, setQuery] = React.useState('');
    const [resultList, setResultList] = React.useState<ICharacterInfo[]>([]);
    const [errorMessage, setErrorMessage] = React.useState("");
    const [loading, setLoading] = React.useState(false);
    const [currentPage, setCurrentPage] = React.useState(1);
    const [total, setTotal] = React.useState(0);

    const onChange = (value: any) => {
        setQuery(value.target.value)
    }

    const fetchResults = async () => {
        setLoading(true)
        try {
           await fetch(`${API}people/?page=${currentPage}&search=${query}`)
            .then(response => response.json())
            .then(data => {
                setTotal(data.count);
            setResultList(data.results)
            })
        } catch (error) {
            setErrorMessage(error)
        }
        finally{
            setLoading(false)
           
        }
    }

    React.useEffect(() => {
        fetchResults();
    },[query, currentPage])

    const handlePagination = (page: number) => { 
        setCurrentPage(page);
    }

    return (
        <Spin spinning={loading}>
        <main className={style.mainContainer}>
            <Header/>
           
            <section className={style.searchInputSection}>
                <Row type="flex" justify="center">
                    <Col span={24} lg={10}>
                        <Search className={style.searchInput} onChange={onChange} allowClear placeholder="Who are you looking for in our archives?"/>
                        {errorMessage && (
                            <Alert closable type="error" showIcon message={errorMessage}/>
                        )}
                    </Col>
                </Row>
            </section>
            <section className={style.results}>
        
                {resultList.length > 0 ? (
                    <>
                    {resultList?.map(character => (
                        <Row key={character.name} gutter={[0, 100]} type="flex" justify="center">
                        <Col xs={24} lg={10}>
                        <CharacterCard character={character}/>
                        </Col>
                        </Row>
                ))}
                    </>
                ) : (
                    <>
                   {!loading && (<EmptyResult/>)}
                    </>
                )}
            </section>
            <Row  type="flex" justify="center">
                    <Col xs={24} lg={12}>
                    <Pagination
                    className={style.pagination}
                    current={currentPage}
                    total={total}
                    onChange={handlePagination}
                    ></Pagination>
                    </Col>
                </Row>
        </main>
        </Spin>

    )
}