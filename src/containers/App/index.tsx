import { Layout } from 'antd';
import * as React from 'react';
import { Route, Switch } from "react-router-dom";

import { Home } from '../Home';

require('./style.scss');
import 'antd/dist/antd.css';

export function App() {
	return (
		<Layout>
			<Layout>
				<Switch>
					<>
						<Route exact path="/" component={Home} />
					</>
			</Switch>
			</Layout>
		</Layout>
	);
}
