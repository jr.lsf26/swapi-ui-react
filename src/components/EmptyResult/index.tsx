import * as React from 'react';

const style = require("./style.scss");

export const EmptyResult = () => { 

    return (
       <div className={style.emptyBox}>
           <img src={require("../../assets/imgs/empty-result.gif")}/>
       </div>
       )
}