import { Card, Spin } from 'antd';
import * as React from 'react';

import { ICharacterInfo } from '../../utils/interfaces';

import { CharacterBasicInfo } from '../CharacterBasicInfo';
import { StarshipInfo } from '../StarshipsInfo';

const style = require('./style.scss');

interface ICardProps{
    character?: ICharacterInfo;
}

export const CharacterCard = ({character} : ICardProps) => { 

    const [currentTab, setCurrentTab] = React.useState<any>('info');
    const [loading, setLoading] = React.useState(false);
    const [starshipsResults, setStarshipsResults] = React.useState([]);
    const [errorMessage, setErrorMessage] = React.useState("");
    const starships: any[] = [];
    const [imgSrc, setImgSrc] = React.useState('')

    const SEARCH_URL = `https://www.googleapis.com/customsearch/v1?key=${process.env.SEARCH_API_KEY}&cx=${process.env.CUSTOM_SEARCH_ID}&q=${character.name}&searchType=image`

    const onTabChange = (key: string, type: string) => { 
        setCurrentTab(key)
    }

    const tabs = [
        {
            key: "info",
            tab: "Info"
        },
        {
            key: "starships",
            tab: "Starships"
        }
    ]

    const content: {[key: string]: React.ReactNode} = {
        info: (<CharacterBasicInfo character={character}/>),
        starships: (<StarshipInfo errorMessage={errorMessage} starships={starshipsResults}/>),
    };

    const fetchImages = () => { 
        setLoading(true)
        try {
            fetch(SEARCH_URL)
            .then(response => response.json())
            .then(data =>{
                setImgSrc(data.items[0].link)
            }) 
        } catch (error) {
            setErrorMessage(error.message);
        }
        finally{
            setLoading(false)
        }
    }

    const fetchStarships = () => { 
        setLoading(true)
        try {
            character?.starships.forEach( url => (
             fetch(url)
                .then(response => response.json())
                .then(data => {
                    starships.push(data)
                })
            ))
            setStarshipsResults(starships);
        } catch (error) {
            setErrorMessage(error.message);
        }
        finally{
            setLoading(false)
        }
    }
    
    React.useEffect(() => { 
        fetchStarships();
        fetchImages();
    }, [])

    return (
        <Spin spinning={loading}>
            <div className={style.cardbox}>
            <img className={style.profile} src={imgSrc ? imgSrc : require('../../assets/imgs/not-found.png')}/>
            <Card 
            className={style.infoCard} 
            title={character.name}
            activeTabKey={currentTab}
            onTabChange={(key: string) => onTabChange(key, 'key')}
            tabList={tabs}
            >
            {content[currentTab]}  
            </Card>
            </div>
        </Spin>
       
    )

}