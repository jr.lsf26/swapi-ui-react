import * as React from 'react';
import { Alert, Row, Col, Pagination, Icon } from 'antd';
import { LabelWithValue } from '../LabelWithValue';

const style = require("./style.scss");

interface IStarshipsProps{ 
    starships: any[];
    errorMessage: string;
}

export const StarshipInfo = ({starships, errorMessage} : IStarshipsProps) => { 

    return (
        <>
            {errorMessage && (<Alert type="error" closable showIcon message={errorMessage}/>)}
            <div className={style.starshipsBox}>
                {starships.length === 0 ? (
                    <h4>This character has no starships</h4>
                ): (
                    <>
                    {starships.map(starship => 
                        (
                        <>
                        <Row>
                            <Col span={1}>
                            <Icon className={style.icon} type="rocket"/>
                            </Col>
                            <Col span={6}>
                            <h4>{starship.name}</h4>
                            </Col>
                        </Row>
                        <Row key={starship.name}>
                            <Col span={12}>
                                <LabelWithValue
                                label="Model"
                                value={starship.model}
                                />
                            </Col>
                            <Col span={12}>
                                <LabelWithValue
                                label="Class"
                                value={starship.starship_class}
                                />
                            </Col>
                        </Row>
                        </>)
                        )}                    
                    </>
                )}
                
            </div>
       </>
       )
}