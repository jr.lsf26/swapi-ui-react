import * as React from 'react';

const style = require('./style.scss');

interface IProps { 
    label: string
    value: string
}

export const LabelWithValue = ({label, value}: IProps) => {
    return (
        <div className={style.box}>
            <label>{label}</label>
            <p>{value}</p>
        </div>
    )
}