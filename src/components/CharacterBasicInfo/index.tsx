import * as React from 'react';
import { ICharacterInfo } from '../../utils/interfaces';
import {heightFormatter, massFormatter, capitalize} from '../../utils/formatters';
import { Row, Col } from 'antd';
import { LabelWithValue } from '../LabelWithValue';

const style = require('./style.scss');

interface ICharacterBasicInfo {
    character: ICharacterInfo;
}

export const CharacterBasicInfo = ({character}: ICharacterBasicInfo) => { 

    return (
        <div className={style.infoBox}>
            <Row gutter={[20, 16]}>
                <Col span={6}>
                    <LabelWithValue
                    label="Height"
                    value={heightFormatter(character.height)}
                    />
                </Col>
                <Col span={6}>
                <LabelWithValue
                    label="Mass"
                    value={massFormatter(character.mass)}
                    />
                </Col>
                <Col span={6}>
                <LabelWithValue
                    label="Birth Year"
                    value={capitalize(character.birth_year)}
                    />
                </Col>
                <Col span={6}>
                <LabelWithValue
                    label="Gender"
                    value={capitalize(character.gender)}
                    />
                </Col>
            </Row>
        </div>
    )
}