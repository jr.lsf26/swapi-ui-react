import * as React from 'react';

const style = require('./style.scss');

export const Header = () => { 
    return(
        <header className={style.header}>
            <h1>jedi archives - a swapi ui</h1>
        </header>
    )
}