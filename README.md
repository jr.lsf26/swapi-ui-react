# Jedi Archives - A Simple SWAPI UI 

This simple interface is focused on displaying a few details about Star Wars
universe characters provided by the SWAPI - Star Wars API. 


## Specifications:
    - React v16.12
    - Ant Design v3.26.7 
    - Sass
    - Typescript v3.75.2


## Getting started 

Run `npm i` to install dependencies after cloning. You can create your .env file to specify your Google Custom Search API Key and your Custom Search ID as following: 
`SEARCH_API_KEY=
CUSTOM_SEARCH_ID=`
After that, run `npm run dev` and navigate to `http://localhost:8080/`. This app has hot-reload enabled.